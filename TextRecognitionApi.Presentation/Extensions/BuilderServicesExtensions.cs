﻿using Microsoft.Extensions.DependencyInjection.Extensions;
using TextRecognitionApi.Infraestructure;
using TextRecognitionApi.Infraestructure.Interfaces;

namespace TextRecognitionApi.Presentation.Extensions
{
    public static class BuilderServicesExtensions
    {

        public static WebApplicationBuilder RegisterServicesAndMappers(this WebApplicationBuilder builder)
            => builder
                .RegisterCommonServices()
                .RegisterHttpClients()
                .RegisterSingletons()
                .RegisterServices()
                .RegisterMappers();

        private static WebApplicationBuilder RegisterCommonServices(this WebApplicationBuilder builder)
        {
            builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();

            builder.Services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            

            return builder;
        }

        private static WebApplicationBuilder RegisterHttpClients(this WebApplicationBuilder builder)
        {
            

            return builder;
        }

        private static WebApplicationBuilder RegisterSingletons(this WebApplicationBuilder builder)
        {

            return builder;
        }

        private static WebApplicationBuilder RegisterServices(this WebApplicationBuilder builder)
        {
        

            return builder;
        }

        private static WebApplicationBuilder RegisterMappers(this WebApplicationBuilder builder)
        {
            /*TODO: Add mappers here... 
            builder.Services.AddAutoMapper(typeof(TodoProfileDTO));
            */
            return builder;
        }
    }
}
