﻿using System.Linq.Expressions;

using Microsoft.EntityFrameworkCore;
using TextRecognitionApi.Domain;
using TextRecognitionApi.Infraestructure.Interfaces;

namespace TextRecognitionApi.Infraestructure.Repositories
{
    public abstract class RepositoryBase<T> : IRepositoryBase<T> where T : BaseEntity
    {
        public RepositoryBase(
            AppDbContext appDbContext)
        {
            DbContext = appDbContext;            
        }

        protected AppDbContext DbContext { get; }

        public virtual IQueryable<T> FindAll()
            => DbContext.Set<T>().AsNoTracking();

        public virtual IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression)
            => DbContext.Set<T>()
                .Where(expression)
                .AsNoTracking();

        public virtual void Create(T entity)
            => DbContext.Set<T>()
                .AddAsync(entity);

        public virtual void Create(List<T> entities)
            => DbContext.Set<T>()
                .AddRangeAsync(entities);

        public virtual void Update(T entity)
            => DbContext.Set<T>()
                .Update(entity);

        public virtual void Delete(T entity)
            => DbContext.Set<T>()
                .Remove(entity);

        public virtual void SoftDelete(T entity)
        {
            entity.DeletedAt = DateTime.Today;
            DbContext.Update(entity);
        }
    }
}
